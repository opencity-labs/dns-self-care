import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
// import LanguageDetector from 'i18next-browser-languagedetector';
// import detectLanguageCustom from './_helpers/detectLanguage';
// const languageDetector = new LanguageDetector();
// languageDetector.addDetector(detectLanguageCustom);

// the translations
// (tip move them in a JSON file and import them,
// or even better, manage them separated from your code: https://react.i18next.com/guides/multiple-translation-files)

const resources = {
  en: {
    translation: {
      lang: 'ENG',
      temporary_domain: 'Temporary Domain',
      temporary_domain_desc: 'Enter the temporary address you are using now',
      production_domain: 'Production Domain',
      production_domain_desc: 'Enter the production address',
      confirm: 'Confirm',
      dnsselfcare_page_description: 'On this page you will find the information to configure and verify DNS',
      website: 'Website',
      personalArea: 'Personal Area',
      typeConfigDesc: "Select the application you are configuring",
      selectOption: "Select an option",
      howTo: "How to config DNS",
      configOk: "All Good: Configuration Confirmed",
      createCnameRecord: 'Create a CNAME record',
      cnameRecord: 'CNAME record',
      withValue: 'with value',
      createRecordA: 'Create type A record',
      aRecord: 'Record A',
      withValueIp: 'with value',
      tempDomainInvalid: "The temporary site address entered is incorrect; it is not an active site.",
      done: "Ok",
      more: "More details",
      warn: "Warning",
      actualDataDesc: "The following records are currently configured:",
      wrongConfig: "An incorrect configuration was found.",
      missingConfig: "Missing configuration",
      name: "Name",
      data: "Value",
      update: "Refresh"
    }
  },
  it: {
    translation: {
      lang: 'ITA',
      temporary_domain: 'Dominio Provvisorio',
      temporary_domain_desc: "Inserisci l'indirizzo temporaneo che stai usando adesso",
      production_domain: 'Dominio Produzione',
      production_domain_desc: "Inserisci l'indirizzo di produzione",
      confirm: 'Conferma',
      dnsselfcare_page_description: 'In questa pagina trovi le informazioni per configurare e verificare i DNS',
      website: 'Sito del Comune',
      personalArea: 'Area Personale',
      typeConfigDesc: "Scegli l'applicazione che stai configurando",
      selectOption: "Scegli un'opzione",
      howTo: "Operazioni necessarie da effettuare",
      configOk: "La configurazione risulta corretta",
      createCnameRecord: 'Crea un record di tipo CNAME',
      cnameRecord: 'Record di tipo CNAME',
      withValue: 'con contenuto',
      createRecordA: 'Crea record di tipo A',
      aRecord: 'Record di tipo A',
      withValueIp: 'con contenuto',
      tempDomainInvalid: "L'indirizzo del sito temporaneo inserito non è corretto, non è un sito attivo.",
      done: "Ok",
      more: "Maggiori dettagli",
      warn: "Attenzione",
      actualDataDesc: "Di seguito trovi la configurazione rilevata in questo monento:",
      wrongConfig: "E' stata individuata una configurazione errata relativa a questo record.",
      missingConfig: "Non è sata individuata nessuna configurazione relativa a questo record",
      name: "Nome",
      data: "Valore",
      update: "Aggiorna"
    }
  }
};

i18n
  // .use(languageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    fallbackLng: 'it',
    interpolation: {
      escapeValue: false // react already safes from xss
    },
    // detection: {
    //   // order and from where user language should be detected
    //   order: ['custom', 'htmlTag','localStorage'], // keys or params to lookup language from
    //   lookupLocalStorage: 'defaultLocale',
    //   lookupSessionStorage: 'defaultLocale'
    // }
  });

export default i18n;
