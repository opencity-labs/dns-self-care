import {
  DropdownMenu,
  DropdownToggle,
  Header,
  HeaderBrand,
  HeaderContent,
  HeaderRightZone,
  Icon,
  LinkList,
  LinkListItem,
  UncontrolledDropdown,
} from "design-react-kit";
import React from "react";
import { useTranslation } from "react-i18next";

const MenuHeader = ({ onLangChange }) => {
  const { t, i18n } = useTranslation();

  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  };
  return (
    <Header theme="" type="slim">
      <HeaderContent>
        <HeaderBrand responsive>OpenCity Labs</HeaderBrand>
        <HeaderRightZone>
          <UncontrolledDropdown nav tag="div">
            <DropdownToggle color="primary" caret nav>
              {t("lang")}
              <Icon
                className="d-none d-lg-block"
                color="icon-white"
                icon="it-expand"
              />
            </DropdownToggle>
            <DropdownMenu className="dark">
              <LinkList>
                <LinkListItem onClick={() => changeLanguage("it")}>
                  ITA
                </LinkListItem>
                <LinkListItem onClick={() => changeLanguage("en")}>
                  ENG
                </LinkListItem>
              </LinkList>
            </DropdownMenu>
          </UncontrolledDropdown>
        </HeaderRightZone>
      </HeaderContent>
    </Header>
  );
};
export default MenuHeader;
