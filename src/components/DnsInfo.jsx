import React, { useEffect, useRef, useState } from "react";
import { Alert, Button, Col, Row, Spinner } from "design-react-kit";
import { useTranslation } from "react-i18next";
import axios from "axios";
import CardRecordInfo from "./CardRecordInfo";

const DnsInfo = ({ dnsProd, dnsTest, typeConfig, refreshData:updatedForm, scrollView }) => {
  const { t } = useTranslation();
  const [dataProd, setDataProd] = useState({});
  const [dataProdA, setDataProdA] = useState({});
  const [dataTest, setDataTest] = useState({});
  const [refreshData, setRefreshData] = useState(updatedForm)
  const [error, setError] = useState("");
  const [results, setResults] = useState(false)
  const [loading, setLoading] = useState(false)

  const dnsResolve = async (name, setData, callback) => {
    return axios
      .get("https://dns.google/resolve", {
        params: {
          name: encodeURIComponent(name),
          type: "A",
        },
      })
      .then((res) => {
        const Answer = res.data?.Answer;
        const recA = Answer?.filter((r) => r.type === 1);
        setData({ Answer, recA });
        callback && callback(Answer);
      })
      .catch((err) => {
        setError(err.message);
      });
  };

  const checkTempDomain = (Answer) => {
    if (!Answer) {
      setError(t("tempDomainInvalid"));
    }
  };

  const getResults = () => {
    if (!cnameCheck()) return false
    if (typeConfig === "website") {
      if (!dataTest.recA) return false
      for (let i = 0; dataTest.recA.lenght; i++) {
        if (!recACheck(dataTest.recA[i].data)) return false
      }
    }
    return true    
  }

  useEffect(() => {
    setResults(getResults())
  }, [dataTest, dataProdA, dataProd])
  

  const afterRequests = () => {
    setLoading(false)
    scrollView && scrollView()
  }

  useEffect(() => {
    setError("");
    const testReq = dnsResolve(dnsTest, setDataTest, checkTempDomain);
    const prodReq = dnsResolve(`${typeConfig === "website" ? "www." : ""}${dnsProd}`, setDataProd);
    const aReq = dnsResolve(dnsProd, setDataProdA); // typeConfig === "website"
    setLoading(true)
    if (typeConfig === "website") {
      Promise.all([testReq, prodReq, aReq]).then(() => {
        afterRequests();
      });
    } else {
      Promise.all([testReq, prodReq]).then(() => {
        afterRequests();
      });
    }
  }, [dnsProd, dnsTest, typeConfig, refreshData]);

  const cnameCheck = () => {
    const result = dataProd.Answer?.find((r) => r.data.includes(dnsTest));
    return Boolean(result);
  };

  const recACheck = (ip) => {
    const result = dataProdA.Answer?.find((r) => r.data.includes(ip));
    return Boolean(result);
  };

  if (error) {
    return <Alert color="danger">{error}</Alert>;
  }

  return (
    <div className="min-vh-100">
      <Row>
        <Col xs={"12"} md={11}>
          <h2>{Boolean(results) ? t("configOk") : t("howTo")}</h2>
        </Col>
        <Col xs={"12"} md={1}>
          <Button
            size="xs"
            color="secondary"
            className="mt-3"
            style={{ float: "right" }}
            onClick={() => setRefreshData(Math.random())}
          >
            {t("update")}
          </Button>
        </Col>
      </Row>
      <Row>
        {Boolean(loading) && (<div class="d-flex justify-content-center"><Spinner active /></div>)}
        {Boolean(dataProd && dataTest && !loading) && (
          <div className="_font-serif">
            <Col xs={"12"}>
              <CardRecordInfo
                checked={cnameCheck()}
                actualData={dataProd.Answer}
                type={5}
                recordFor={`${typeConfig === "website" ? 'www.' : ''}${dnsProd}`}
                filterResultsByType={typeConfig === "website"}
              >
                {cnameCheck() ? t("cnameRecord") : t("createCnameRecord")}
                <br />
                <span className="font-monospace text-break">
                  <b>{`${typeConfig === "website" ? 'www.' : ''}${dnsProd}`}</b>
                </span>{" "}
                {t("withValue")}{" "}
                <span className="font-monospace text-break">
                  <b>{dnsTest}</b>
                </span>
              </CardRecordInfo>
              {Boolean(typeConfig === "website") &&
                dataTest.recA?.map((r, i) => (
                  <CardRecordInfo
                    key={i}
                    checked={recACheck(r.data)}
                    actualData={dataProdA.recA}
                    type={1}
                    recordFor={dnsProd}
                    filterResultsByType={typeConfig === "website"}
                  >
                    <div>
                      {recACheck(r.data) ? t("aRecord") : t("createRecordA")}
                      <br />
                      <span className="font-monospace text-break">
                        <b>{dnsProd}</b>
                      </span>{" "}
                      {t("withValueIp")}{" "}
                      <span className="font-monospace text-break">
                        <b>{r.data}</b>
                      </span>
                    </div>
                  </CardRecordInfo>
                ))}
            </Col>
          </div>
        )}
      </Row>
    </div>
  );
};

export default DnsInfo;
