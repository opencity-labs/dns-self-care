import React, { useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { Button, FormGroup, Input, Label } from "design-react-kit";
import { useTranslation } from "react-i18next";
import { useQueryParams } from "../helpers/utilities";
import { useSearchParams } from "react-router-dom";

const FormDNS = ({onFormSubmit}) => {
  const { t } = useTranslation();
  const { dnsTest:testDomain, dnsProd:prodDomain, typeConfig:type } = useQueryParams()
  const [typeConfig, setTypeConfig] = useState(type||"");
  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    mode: "onChange",
    defaultValues: {
      dnsTest: testDomain || "",
      dnsProd: prodDomain || ""
    },
  });
  const [searchParams, setSearchParams] = useSearchParams();
  const onSubmit = (data) => {
    data.dnsProd = data.dnsProd.replace(/^(www\.)/,"");
    data.dnsTest = data.dnsTest.replace(/^(www\.)/,"");
    setSearchParams({...data, typeConfig})
    onFormSubmit({...data, typeConfig})
  }
  return (
    <fieldset key={t("lang")} aria-label="Form dns">
      <legend>{t("typeConfigDesc")}</legend>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="form-group">
          <FormGroup check inline>
            <Input
              name="typeConfig"
              type="radio"
              id="website"
              onChange={() => {
                setTypeConfig("website");
              }}
              checked={typeConfig==="website"}
            />
            <Label check htmlFor="website">
              {t("website")}
            </Label>
          </FormGroup>
          <FormGroup check inline>
            <Input
              name="typeConfig"
              type="radio"
              id="personalArea"
              onChange={() => {
                setTypeConfig("personalArea");
              }}
              checked={typeConfig==="personalArea"}
            />
            <Label check htmlFor="personalArea">
              {t("personalArea")}
            </Label>
          </FormGroup>
        </div>

        <div className="form-group">
          <Controller
            name="dnsTest"
            control={control}
            rules={{ required: true }}
            render={({ field }) => (
              <Input
                type="text"
                label={t("temporary_domain")}
                id={"dnsTest"}
                placeholder=""
                disabled={!typeConfig}
                infoText={t("temporary_domain_desc")}
                invalid={errors.dnsTest?.type === "required" ? true : false}
                innerRef={field.ref}
                value={field.value}
                onChange={field.onChange}
              />
            )}
          />
        </div>
        <div className="form-group">
          <Controller
            name="dnsProd"
            control={control}
            rules={{ required: true }}
            render={({ field }) => (
              <Input
                type="text"
                label={t("production_domain")}
                id="dnsProd"
                disabled={!typeConfig}
                placeholder=""
                infoText={t("production_domain_desc")}
                invalid={errors.dnsProd?.type === "required" ? true : false}
                innerRef={field.ref}
                value={field.value}
                onChange={field.onChange}
              />
            )}
          />
        </div>
        <Button type="submit" color="primary" className="mt-3">
          {t("confirm")}
        </Button>
      </form>
    </fieldset>
  );
};
export default FormDNS;
