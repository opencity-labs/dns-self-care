import React, { useEffect, useState } from "react";
import {
  Accordion,
  AccordionBody,
  AccordionHeader,
  AccordionItem,
  Callout,
  CalloutText,
  CalloutTitle,
  Icon,
} from "design-react-kit";
import { useTranslation } from "react-i18next";

const CardRecordInfo = ({ checked, children, actualData, type, recordFor, filterResultsByType }) => {
  const { t } = useTranslation();
  const [collapseElementOpen, setCollapseElement] = useState("");
  const [actualRecords, setActualRecords] = useState([])
  useEffect(() => {
    if (!checked) {
      setCollapseElement("1");
    } else {
      setCollapseElement("");
    }
  }, [checked, children]);

  useEffect(() => {
    setActualRecords(
      actualData 
        ? actualData.filter((r)=>{
          if (filterResultsByType) return r.type === type
          return true
        })
        : []
      )
  }, [actualData, type, recordFor])

  return (
    <Callout
      color={checked ? "success" : ""}
      highlight
      className="mw-100 my-5 py-3 bg-white rounded shadow "
    >
      <CalloutTitle>
        {checked ? (
          <>
            <Icon aria-hidden icon="it-check-circle" />
            {t("done")}
          </>
        ) : (
          <Icon aria-hidden icon="it-info-circle" />
        )}
      </CalloutTitle>
      {children}
      {Boolean(!checked && actualRecords.length > 0) && (
        <Accordion className="mt-4 me-5">
          <AccordionItem>
            <AccordionHeader
              active={collapseElementOpen === "1"}
              onToggle={() =>
                setCollapseElement(collapseElementOpen !== "1" ? "1" : "")
              }
              className={checked ? "" : "text-warning"}
            >
              {checked ? (
                t("more")
              ) : (
                <>
                  <Icon aria-hidden color="warning" icon="it-warning-circle" />
                  {t("warn")}
                </>
              )}
            </AccordionHeader>
            <AccordionBody active={collapseElementOpen === "1"}>
              {Boolean(!checked && actualRecords.length > 0 ) && <div>{t("wrongConfig")}</div>}
              {Boolean(!checked && actualRecords.length === 0) && (
                <div>{t("missingConfig")}</div>
              )}
              {Boolean(actualRecords.length > 0) && (
                <>
                  {t("actualDataDesc")}
                  {actualRecords.map((record, i) => (
                    <div
                      key={i}
                      className={`py-4 ${
                        actualData.length - 1 !== i ? "border-bottom" : "pb-0"
                      }`}
                    >
                      <p>
                        {record.type === 1
                          ? t("aRecord")
                          : record.type === 5
                          ? t("cnameRecord")
                          : "Record"}
                        <br />
                        <span className="font-monospace text-break">
                          <b>{record.name}</b>
                        </span>
                        <span>{" "}{t("withValue")}{" "}</span>
                        <span className="font-monospace text-break">
                          <b>{record.data}</b>
                        </span>
                      </p>
                    </div>
                  ))}
                </>
              )}
            </AccordionBody>
          </AccordionItem>
        </Accordion>
      )}
    </Callout>
  );
};

export default CardRecordInfo;
