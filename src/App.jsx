import React from 'react';
import './style/App.scss';
import 'bootstrap-italia/dist/css/bootstrap-italia.min.css';
import 'typeface-titillium-web';
import 'typeface-roboto-mono';
import 'typeface-lora';
import DNSSelfCarePage from './pages/DNSSelfCarePage';
import MenuHeader from './components/MenuHeader';
import { BrowserRouter as Router } from 'react-router-dom';

const App = () => (
  <div>
    <MenuHeader />
    <Router>
      <DNSSelfCarePage />
    </Router>
  </div>
)
export default App;