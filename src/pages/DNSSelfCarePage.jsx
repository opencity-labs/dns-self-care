import React, { useEffect, useState, useRef } from "react";
import { Container, Row, Section } from "design-react-kit";
import FormDNS from "../components/FormDNS";
import { useTranslation } from "react-i18next";
import DnsInfo from "../components/DnsInfo";

const DNSSelfCarePage = () => {
  const { t } = useTranslation();
  const [form, setForm] = useState({});
  const [refreshData, setRefreshData] = useState();
  const dnsInfoRef = useRef(null);

  const scrollView = () => {
    if ((form.dnsTest, form.dnsProd, form.typeConfig)) {
      window.scrollTo({
        top: dnsInfoRef.current.offsetTop,
        behavior: "smooth",
      });
    }
  }

  const onFormSubmit = (data) => {
    setRefreshData(Math.random());
    setForm(data);
  };
  return (
    <>
      <Section>
        <Container>
          <Row>
             <div>
              <h1>DNS Self Care</h1>
              <p>{t("dnsselfcare_page_description")}</p>
              <FormDNS onFormSubmit={(data) => onFormSubmit(data)} />
            </div>
          </Row>
        </Container>
      </Section>
      <div ref={dnsInfoRef}>
        {Boolean(form.typeConfig) && (
          <Section color="muted">
            <Container>
              <DnsInfo {...form} refreshData={refreshData} scrollView={scrollView}/>
            </Container>
          </Section>
        )}
      </div>
    </>
  );
};

export default DNSSelfCarePage;
