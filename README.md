# DNS Self-care

Una semplice single-page app che sia di supporto ai nostri clienti quando è necessario configurare il DNS secondo le nostre indicazioni 

## destinatari
 
La pagina è rivolta al personale tecnico che lavora negli enti nostri clienti: loro ricevono istruzioni via email su come configurare il DNS quando devono mettere in produzione il sito istituzionale o l'area personale.

## come funziona

Il tecnico seleziona se sta configurando il sito o l'area personale; fatta questa scelta gli viene chiesto l'indirizzo di produzione e l'indirizzo temporaneo che sta usando adesso.

Sulla pagina compaiono allora le istruzioni su come deve essere configurato il DNS.

A questo punto abbiamo tutte le informazioni per fare dei controlli sulla pagina e aiutare il tecnico al massimo delle nostre possibilità.

Dobbiamo mettere in evidenza sulla pagina un feedback sui record DNS configurati. Potranno verificarsi vari casi:

1. la configurazione non è stata ancora fatta
2. la configurazione è presente ma NON è corretta
3. la configurazione è corretta 

## Dettagli tecnici

Le due configurazioni da controllare Someone

#### 1) Sito Web

Es: 

Dominio produzione: `comune.vicopisano.pi.it`
Dominio temporaneo: `opencityvicopisanopnrr.openpa.opencontent.io`

```
istruzioni

Crea un record CNAME per il record

www.comune.vicopisano.pi.it

con valore opencityvicopisanopnrr.openpa.opencontent.io

Crea due record A per il record senza prefisso www, es comune.vicopisano.pi.it con valori

* 99.80.82.152
* 99.81.25.153
```

Errori da evidenziare:

- [ ] l'indirizzo del sito temporaneo non è corretto, non è un sito attivo
- [ ] l'indirizzo del sito di produzione non è configurato
- [ ] l'indirizzo del sito di produzione è configurato ma NON è corretto, il valore attuale è XXX invece dovrebbe essere YYY.




#### 2) Area personale / Servizi digitali

Dominio temporaneo: servizi.vicopisano.opencityitalia.it
Dominio produzione: servizi.comune.vicopisano.pi.it


```
istruzioni

Crea un record CNAME per il record

servizi.vicopisano.opencityitalia.it

con valore servizi.vicopisano.opencityitalia.it

```

## Quick Overview

Inside the project you can run some built-in commands:

### `yarn install`

and for start

### `yarn start`

Runs the app in development mode.<br>

Build:

### `yarn run build`


# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
